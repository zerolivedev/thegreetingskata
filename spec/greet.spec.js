const { greet } = require("../lib/greet")
const { expect } = require("chai")

describe("greet", () => {
  it("greets to a friend when name is not provided", () => {

    const simpleGreet = greet(null)

    expect(simpleGreet).to.eq("Hello, my friend.")
  })

  it("greets to a person by name", () => {

    const simpleGreet = greet("Akira")

    expect(simpleGreet).to.eq("Hello, Akira.")
  })

  it("greets to another person by name", () => {

    const simpleGreet = greet("Charlie")

    expect(simpleGreet).to.eq("Hello, Charlie.")
  })

  it("greets shouting when the person's name is shouted", () => {

    const shoutedGreet = greet("SAM")

    expect(shoutedGreet).to.eq("HELLO SAM!")
  })

  it("greets shouting when the another person's name is shouted", () => {

    const shoutedGreet = greet("AKIRA")

    expect(shoutedGreet).to.eq("HELLO AKIRA!")
  })

  it("greets to two persons by their names", () => {

    const multipleGreet = greet(["Akira", "Charlie"])

    expect(multipleGreet).to.eq("Hello, Akira and Charlie.")
  })

  it("greets to another two persons by their names", () => {

    const multipleGreet = greet(["Akira", "Sam"])

    expect(multipleGreet).to.eq("Hello, Akira and Sam.")
  })

  it("greets to three persons by their names", () => {

    const multipleGreet = greet(["Akira", "Charlie", "Sam"])

    expect(multipleGreet).to.eq("Hello, Akira, Charlie and Sam.")
  })

  it("greets to another three persons by their names", () => {

    const multipleGreet = greet(["Akira", "Sam", "Charlie"])

    expect(multipleGreet).to.eq("Hello, Akira, Sam and Charlie.")
  })

  it("greets to a lot of persons by their names", () => {

    const multipleGreet = greet(["Akira", "Sam", "Zero", "Charlie"])

    expect(multipleGreet).to.eq("Hello, Akira, Sam, Zero and Charlie.")
  })
})
