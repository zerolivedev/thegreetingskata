FROM node:16.17.0-slim

WORKDIR /opt/kata

COPY package* .

RUN npm install

COPY . .
