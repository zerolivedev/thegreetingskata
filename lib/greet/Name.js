
const Name = class {
  constructor(value) {
    this.value = value
  }

  isNull() {
    return (this.value === null)
  }

  isUpperCase() {
    return (this.value.toUpperCase() === this.value)
  }

  toString() {
    return this.value
  }
}

module.exports = { Name }
