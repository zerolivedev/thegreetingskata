const { Name } = require("./Name")

const Greet = class {
  static toMultiple = function(names) {
    if (this._areMoreThanTwo(names)) {
      return this._simpleGreetForMoreThanTwo(names)
    } else {
      return this._simpleGreetForTwo(names)
    }
  }

  static toOne = function(rawName) {
    const name = new Name(rawName)
    if (name.isNull()) { return this._default() }

    let greet
    if (name.isUpperCase()) {
      greet = this._shoutedFor(name)
    } else {
      greet = this._simpleFor(name)
    }
    return greet
  }

  static _simpleGreetForTwo = function(names) {
    return `Hello, ${names[0]} and ${names[1]}.`
  }

  static _simpleGreetForMoreThanTwo = function(names) {
    const lastName = names[names.length - 1]
    let namesInMiddle = ""

    for (let index = 0; index < names.length - 1; index++) {
      namesInMiddle += ", " + names[index]
    }

    return `Hello${namesInMiddle} and ${lastName}.`
  }

  static _default = function() {
    return "Hello, my friend."
  }

  static _simpleFor = function(name) {
    return `Hello, ${name.toString()}.`
  }

  static _shoutedFor = function(name) {
    return `HELLO ${name.toString()}!`
  }

  static _areMoreThanTwo = function(names) {
    return (names.length > 2)
  }
}

module.exports = { Greet }
