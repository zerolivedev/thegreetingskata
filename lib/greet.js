const { Greet } = require("./greet/Greet")

const areMoreThanNameIn = function(names) {
  return Array.isArray(names)
}

const greet = function(names) {
  if (areMoreThanNameIn(names)) {
    return Greet.toMultiple(names)
  } else {
    return Greet.toOne(names)
  }
}

module.exports = { greet }
